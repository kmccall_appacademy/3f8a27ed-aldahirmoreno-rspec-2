def reverser(&prc)
  word = prc.call
  new_word = []
  word.split(" ").each{|x| new_word << x.reverse}
  new_word.join(" ")
end

def adder(num=1,&prc)
  prc.call + num
end

def repeater(reps=1,&prc)
  reps.times { prc.call }
end
