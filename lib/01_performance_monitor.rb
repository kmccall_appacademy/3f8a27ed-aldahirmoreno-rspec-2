def measure(calls=1,&block)
  before = Time.now
  calls.times { block.call }
  after = Time.now
  (after - before)/calls
end
